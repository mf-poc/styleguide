import styled from "styled-components";

interface ButtonProps {
  variant: "primary" | "secondary" | "danger";
}

function getStyles(props: ButtonProps): string {
  switch (props.variant) {
    case "primary":
      return `
    background-color: var(--color-primary);
    color: var(--color-white);
  `;
    case "secondary":
      return `
    background-color: var(--color-secondary);
    color: var(--color-black);
  `;
    case "danger":
      return `
    background-color: var(--color-danger);
    color: var(--color-white);
  `;
  }
}

export const Button = styled.button<{ variant: ButtonProps["variant"] }>`
  align-items: center;
  border-radius: 5px;
  border: none;
  box-sizing: border-box;
  cursor: pointer;
  display: inline-flex;
  flex-flow: row nowrap;
  justify-content: center;
  padding: 5px 10px;
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;

  ${({ variant }) => getStyles({ variant })}
`;
