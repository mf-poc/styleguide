const { mergeWithRules } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react-ts");

const mergeRulesWithMatchingTest = mergeWithRules({
  module: {
    rules: {
      test: "match",
      use: {
        loader: "match",
        options: "replace",
      },
    },
  },
});

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "mf-poc",
    projectName: "styleguide",
    webpackConfigEnv,
    argv,
  });

  const config = mergeRulesWithMatchingTest(defaultConfig, {
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: [
            { loader: require.resolve("style-loader") },
            {
              loader: require.resolve("css-loader"),
              options: { modules: true },
            },
          ],
        },
      ],
    },
  });

  return config;
};
